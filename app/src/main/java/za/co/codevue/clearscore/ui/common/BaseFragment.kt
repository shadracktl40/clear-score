package za.co.codevue.clearscore.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<VB : ViewDataBinding> : Fragment() {
    @LayoutRes
    abstract fun getLayoutRes(): Int

    protected lateinit var viewBinding: VB

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewBinding = DataBindingUtil.inflate(
            inflater,
            getLayoutRes(),
            container,
            false
        )
        return viewBinding.root
    }

    override fun onDestroyView() {
        // avoid leaking views
        (requireActivity() as BaseActivity<*>).dismissDialog()
        super.onDestroyView()
    }
}