package za.co.codevue.clearscore.ui.creditreport.detailed

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import za.co.codevue.model.CreditReport
import za.co.codevue.shared.parcelize.CreditReportParceler

@Parcelize
data class DetailedReportArgs(
    val creditReport: @WriteWith<CreditReportParceler>() CreditReport
) : Parcelable
