package za.co.codevue.clearscore.ui.creditreport.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import za.co.codevue.model.CreditReport
import za.co.codevue.shared.domain.creditreport.GetCreditReportUseCase
import za.co.codevue.shared.result.Result
import za.co.codevue.shared.result.failed
import za.co.codevue.shared.result.isLoading
import za.co.codevue.shared.result.updateOnSuccess
import za.co.codevue.shared.util.map
import javax.inject.Inject

@HiltViewModel
class CreditReportOverviewViewModel @Inject constructor(
    private val getCreditReportUseCase: GetCreditReportUseCase
) : ViewModel() {
    private val creditReportResult = MutableLiveData<Result<CreditReport>>()
    private val creditReportResultObserver = MediatorLiveData<CreditReport>()

    val loading: LiveData<Boolean> = creditReportResult.map { it.isLoading }
    val creditReport: LiveData<CreditReport> = creditReportResultObserver
    val error: LiveData<Result.Error?> = creditReportResult.map {
        if (it.failed) (it as Result.Error) else null
    }

    init {
        // observe credit report result
        creditReportResultObserver.addSource(creditReportResult) {
            it.updateOnSuccess(creditReportResultObserver)
        }
    }

    fun fetchCreditReport() {
        getCreditReportUseCase(Unit, creditReportResult)
    }
}