package za.co.codevue.clearscore.ui.creditreport.detailed

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import za.co.codevue.model.CreditReport
import javax.inject.Inject

@HiltViewModel
class CreditReportDetailedViewModel @Inject constructor() : ViewModel() {
    val creditReport = ObservableField<CreditReport>()

    fun setCreditReport(creditReport: CreditReport) {
        this.creditReport.set(creditReport)
    }
}