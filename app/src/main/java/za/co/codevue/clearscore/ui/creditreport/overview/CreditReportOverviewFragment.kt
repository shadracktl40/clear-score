package za.co.codevue.clearscore.ui.creditreport.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import za.co.codevue.clearscore.R
import za.co.codevue.clearscore.databinding.FragmentCreditReportOverviewBinding
import za.co.codevue.clearscore.ui.common.BaseFragment
import za.co.codevue.clearscore.ui.creditreport.detailed.DetailedReportArgs

@AndroidEntryPoint
class CreditReportOverviewFragment : BaseFragment<FragmentCreditReportOverviewBinding>() {
    private val viewModel: CreditReportOverviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        initViewBinding()
        viewModel.fetchCreditReport()
        return viewBinding.root
    }

    private fun initViewBinding() {
        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            vm = viewModel
            buttonViewFullReport.setOnClickListener {
                viewModel.creditReport.value?.also {
                    findNavController().navigate(
                        CreditReportOverviewFragmentDirections.startDetailedReportFragment(
                            DetailedReportArgs(it)
                        )
                    )
                }
            }
            errorView.buttonTryAgain.setOnClickListener {
                viewModel.fetchCreditReport()
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_credit_report_overview
}