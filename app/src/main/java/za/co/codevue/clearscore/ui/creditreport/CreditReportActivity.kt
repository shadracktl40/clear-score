package za.co.codevue.clearscore.ui.creditreport

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import za.co.codevue.clearscore.R
import za.co.codevue.clearscore.databinding.ActivityCreditReportBinding
import za.co.codevue.clearscore.ui.common.BaseActivity

@AndroidEntryPoint
class CreditReportActivity : BaseActivity<ActivityCreditReportBinding>() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNavController()
    }

    private fun initNavController() {
        navController = Navigation.findNavController(this, R.id.nav_host)
        viewBinding.toolbarLayout.toolbar.also { toolbar ->
            setSupportActionBar(toolbar)
            navController.also { navController ->
                toolbar.setupWithNavController(navController)
                NavigationUI.setupActionBarWithNavController(this, navController)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.activity_credit_report
}