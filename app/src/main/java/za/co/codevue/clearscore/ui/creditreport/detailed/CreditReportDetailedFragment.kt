package za.co.codevue.clearscore.ui.creditreport.detailed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import za.co.codevue.clearscore.R
import za.co.codevue.clearscore.databinding.FragmentCreditReportDetailedBinding
import za.co.codevue.clearscore.ui.common.BaseFragment

@AndroidEntryPoint
class CreditReportDetailedFragment : BaseFragment<FragmentCreditReportDetailedBinding>() {
    private val args: CreditReportDetailedFragmentArgs by navArgs()
    private val viewModel: CreditReportDetailedViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            vm = viewModel
        }
        viewModel.setCreditReport(args.params.creditReport)
        return viewBinding.root
    }

    override fun getLayoutRes(): Int = R.layout.fragment_credit_report_detailed
}