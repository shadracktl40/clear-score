package za.co.codevue.clearscore.util

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import za.co.codevue.clearscore.R
import za.co.codevue.model.CreditDebt
import za.co.codevue.shared.util.format
import java.math.BigDecimal

/** Shows or hides a [View] */
@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Boolean?) {
    visibility = if (show == true) View.VISIBLE else View.GONE
}


@BindingAdapter("debtAmount")
fun TextView.setDebtAmount(totalDebt: BigDecimal?) {
    totalDebt?.also {
        text = it.format()
    }
}

@BindingAdapter("debtLimitOrTotal")
fun TextView.setDebtLimitOrTotal(debt: CreditDebt?) {
    debt?.also {
        var stringRes = R.string.label_limit
        var amount = it.creditLimit

        if (it.isLongTerm()) {
            stringRes = R.string.label_total
            amount = it.totalDebt
        }

        text = String.format(this.context.getString(stringRes), amount.format())
    }
}