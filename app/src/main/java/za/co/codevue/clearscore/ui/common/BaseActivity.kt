package za.co.codevue.clearscore.ui.common

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {
    @LayoutRes
    abstract fun getLayoutRes(): Int

    protected lateinit var viewBinding: VB
    var dialog: AlertDialog? = null

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // init viewBinding
        viewBinding = DataBindingUtil.setContentView(this, getLayoutRes())
    }

    override fun onDestroy() {
        // avoid leaking views
        dismissDialog()
        super.onDestroy()
    }

    fun dismissDialog() {
        if (dialog?.isShowing == true) {
            dialog?.dismiss()
        }
        dialog = null
    }
}