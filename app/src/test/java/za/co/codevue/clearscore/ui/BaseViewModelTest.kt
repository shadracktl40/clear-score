package za.co.codevue.clearscore.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule

import org.junit.Rule
import za.co.codevue.clearscore.util.SyncTaskExecutorRule

abstract class BaseViewModelTest {
    // Executes tasks in the Architecture Components in the same thread
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Executes tasks in a synchronous [TaskScheduler]
    @get:Rule
    val syncTaskExecutorRule = SyncTaskExecutorRule()
}