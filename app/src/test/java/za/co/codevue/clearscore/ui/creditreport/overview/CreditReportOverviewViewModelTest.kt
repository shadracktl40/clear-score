package za.co.codevue.clearscore.ui.creditreport.overview

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import za.co.codevue.androidtest.shared.util.getOrAwaitValue
import za.co.codevue.clearscore.ui.BaseViewModelTest
import za.co.codevue.model.CreditReport
import za.co.codevue.shared.data.creditreport.repository.ICreditReportRepository
import za.co.codevue.shared.domain.creditreport.GetCreditReportUseCase
import za.co.codevue.shared.result.failed
import za.co.codevue.test.shared.data.tCreditReport

@RunWith(JUnit4::class)
class CreditReportOverviewViewModelTest : BaseViewModelTest() {
    private val repository = mock<ICreditReportRepository>()

    @Test
    fun `should set credit report when fetch credit report succeeds`() {
        // given
        val viewModel = createViewModel()
        whenever(repository.getCreditReport()).thenReturn(tCreditReport)
        // when
        viewModel.fetchCreditReport()
        // then
        assertEquals(tCreditReport, viewModel.creditReport.getOrAwaitValue())
    }

    @Test
    fun `should set error when fetch credit report fails`() {
        // given
        val viewModel = createViewModel(
            getCreditReportUseCase = FailingGetCreditReportUseCase(repository)
        )
        // when
        viewModel.fetchCreditReport()
        // then
        viewModel.error.getOrAwaitValue().also {
            assertTrue(it?.failed == true)
            assertTrue(it?.message?.isNotBlank() == true)
        }
    }

    private fun createViewModel(
        getCreditReportUseCase: GetCreditReportUseCase = GetCreditReportUseCase(repository)
    ): CreditReportOverviewViewModel {
        return CreditReportOverviewViewModel(getCreditReportUseCase)
    }

    /**
     * Use case that returns an error when executed.
     */
    class FailingGetCreditReportUseCase(
        repository: ICreditReportRepository
    ) : GetCreditReportUseCase(repository) {
        override fun execute(parameters: Unit): CreditReport {
            throw Exception("Error!")
        }
    }
}