package za.co.codevue.clearscore.ui.creditreport.detailed

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.test.shared.data.tCreditReport

@RunWith(JUnit4::class)
class CreditReportDetailedViewModelTest {
    @Test
    fun `should set credit report observable field`() {
        // given
        val viewModel = CreditReportDetailedViewModel()
        // when
        viewModel.setCreditReport(tCreditReport)
        // then
        assertEquals(tCreditReport, viewModel.creditReport.get())
    }
}