package za.co.codevue.model

data class CreditReport(
    val creditScore: CreditScore,
    val daysUntilNextReport: Int,
    val hasEverDefaulted: Boolean,
    val longTermDebt: CreditDebt,
    val negativeFactors: Int,
    val positiveFactors: Int,
    val percentageCreditUsed: Int,
    val scoreBandDescription: String,
    val shortTermDebt: CreditDebt
)