package za.co.codevue.model

data class CreditScore(
    val changedScore: Int,
    val currentScore: Int,
    val maxScore: Int,
    val minScore: Int
)