package za.co.codevue.model

import java.math.BigDecimal

/*
 * Weird name but makes sense for this case :p
 */
data class CreditDebt(
    val changeInDebt: BigDecimal,
    val creditLimit: BigDecimal? = null,
    val creditUtilization: Int? = null,
    val totalDebt: BigDecimal
) {
    fun isLongTerm(): Boolean = creditLimit == null && creditUtilization == null

    fun calculateTotalDebt(): BigDecimal {
        return when {
            isLongTerm() -> totalDebt.minus(changeInDebt.abs())
            else -> totalDebt
        }
    }

    fun calculateDebtPercentage(): Int {
        return when {
            isLongTerm() -> {
                ((totalDebt.minus(changeInDebt.abs())
                    .toDouble() / totalDebt.toDouble()) * 100).toInt()
            }
            else -> creditUtilization ?: 0
        }
    }
}
