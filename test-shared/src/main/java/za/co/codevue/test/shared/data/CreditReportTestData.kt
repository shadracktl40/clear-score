package za.co.codevue.test.shared.data

import za.co.codevue.model.CreditDebt
import za.co.codevue.model.CreditReport
import za.co.codevue.model.CreditScore
import java.math.BigDecimal

val tCreditScore = CreditScore(
    changedScore = 0,
    currentScore = 500,
    maxScore = 700,
    minScore = 0
)

val tLongTermDebt = CreditDebt(
    changeInDebt = BigDecimal.ZERO,
    creditLimit = null,
    creditUtilization = null,
    totalDebt = BigDecimal.ONE
)

val tShortTermDebt = CreditDebt(
    changeInDebt = BigDecimal.ZERO,
    creditLimit = BigDecimal.TEN,
    creditUtilization = 10,
    totalDebt = BigDecimal.ONE
)

val tCreditReport = CreditReport(
    creditScore = tCreditScore,
    daysUntilNextReport = 3,
    hasEverDefaulted = true,
    longTermDebt = tLongTermDebt,
    negativeFactors = 0,
    positiveFactors = 4,
    percentageCreditUsed = 10,
    scoreBandDescription = "Good",
    shortTermDebt = tShortTermDebt
)