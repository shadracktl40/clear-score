package za.co.codevue.model

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.test.shared.data.tLongTermDebt
import za.co.codevue.test.shared.data.tShortTermDebt

@RunWith(JUnit4::class)
class CreditDebtTest {
    @Test
    fun `should return true when it is a long term debt`() {
        // when
        val actual = tLongTermDebt.isLongTerm()
        // then
        assertTrue(actual)
    }

    @Test
    fun `should return false when it is a short term debt`() {
        // when
        val actual = tShortTermDebt.isLongTerm()
        // then
        assertFalse(actual)
    }

    @Test
    fun `should return the correct total for a long term debt`() {
        // given
        val debt = tLongTermDebt
        val expected = debt.totalDebt.minus(debt.changeInDebt.abs())
        // when
        val actual = debt.calculateTotalDebt()
        // then
        assertEquals(expected, actual)
    }

    @Test
    fun `should return the correct total for a short term debt`() {
        // given
        val debt = tShortTermDebt
        val expected = debt.totalDebt
        // when
        val actual = debt.calculateTotalDebt()
        // then
        assertEquals(expected, actual)
    }

    @Test
    fun `should return the correct percentage for a long term debt`() {
        // given
        val debt = tLongTermDebt
        val expected = ((debt.totalDebt.minus(debt.changeInDebt.abs())
            .toDouble() / debt.totalDebt.toDouble()) * 100).toInt()
        // when
        val actual = debt.calculateDebtPercentage()
        // then
        assertEquals(expected, actual)
    }

    @Test
    fun `should return the correct percentage for a short term debt`() {
        // given
        val debt = tShortTermDebt
        val expected = debt.creditUtilization
        // when
        val actual = debt.calculateDebtPercentage()
        // then
        assertEquals(expected, actual)
    }
}