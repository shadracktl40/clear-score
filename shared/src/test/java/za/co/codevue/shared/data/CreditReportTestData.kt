package za.co.codevue.shared.data

import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO

val tCreditReportDto = CreditReportDTO(
    changeInLongTermDebt = "-327",
    changeInShortTermDebt = "549",
    changedScore = 0,
    currentLongTermCreditLimit = null,
    currentLongTermCreditUtilisation = null,
    currentLongTermDebt = "24682",
    currentShortTermCreditLimit = "30600",
    currentShortTermCreditUtilisation = 44,
    currentShortTermDebt = "13758",
    daysUntilNextReport = 9,
    equifaxScoreBandDescription = "Excellent",
    hasEverDefaulted = false,
    maxScoreValue = 700,
    minScoreValue = 0,
    monthsSinceLastDefaulted = -1,
    numNegativeScoreFactors = 0,
    numPositiveScoreFactors = 9,
    percentageCreditUsed = 44,
    score = 514
)

val tCreditReportDtoWithNullValues = CreditReportDTO(
    changeInLongTermDebt = null,
    changeInShortTermDebt = null,
    changedScore = null,
    currentLongTermCreditLimit = null,
    currentLongTermCreditUtilisation = null,
    currentLongTermDebt = null,
    currentShortTermCreditLimit = null,
    currentShortTermCreditUtilisation = null,
    currentShortTermDebt = null,
    daysUntilNextReport = null,
    equifaxScoreBandDescription = null,
    hasEverDefaulted = null,
    maxScoreValue = null,
    minScoreValue = null,
    monthsSinceLastDefaulted = null,
    numNegativeScoreFactors = null,
    numPositiveScoreFactors = null,
    percentageCreditUsed = null,
    score = null
)