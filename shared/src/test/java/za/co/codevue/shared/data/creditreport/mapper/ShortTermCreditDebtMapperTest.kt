package za.co.codevue.shared.data.creditreport.mapper

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.shared.data.tCreditReportDto
import za.co.codevue.shared.data.tCreditReportDtoWithNullValues
import za.co.codevue.shared.util.toSafeBigDecimal
import java.math.BigDecimal

@RunWith(JUnit4::class)
class ShortTermCreditDebtMapperTest {
    @Test
    fun `should map CreditReportDTO to CreditDebt`() {
        // given
        val dto = tCreditReportDto
        // when
        val actual = ShortTermCreditDebtMapper.toModel(dto)
        // then
        with(actual) {
            assertEquals(
                dto.changeInShortTermDebt?.toSafeBigDecimal(),
                changeInDebt
            )
            assertEquals(
                dto.currentShortTermCreditLimit?.toSafeBigDecimal(),
                creditLimit
            )
            assertEquals(
                dto.currentShortTermCreditUtilisation,
                creditUtilization
            )
            assertEquals(
                dto.currentShortTermDebt?.toSafeBigDecimal(),
                totalDebt
            )
        }
    }

    @Test
    fun `should map CreditReportDTO to CreditDebt with default values`() {
        // given
        val dto = tCreditReportDtoWithNullValues
        // when
        val actual = ShortTermCreditDebtMapper.toModel(dto)
        // then
        with(actual) {
            assertEquals(BigDecimal.ZERO, changeInDebt)
            assertEquals(BigDecimal.ZERO, creditLimit)
            assertEquals(0, creditUtilization)
            assertEquals(BigDecimal.ZERO, changeInDebt)
        }
    }
}