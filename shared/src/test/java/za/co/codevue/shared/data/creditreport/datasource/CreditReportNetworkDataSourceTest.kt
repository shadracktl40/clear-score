package za.co.codevue.shared.data.creditreport.datasource

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.shared.api.creditreport.ICreditReportApiService
import za.co.codevue.shared.data.BaseNetworkDataSourceTest
import za.co.codevue.shared.data.tCreditReportDto
import za.co.codevue.shared.result.Result.ServerException

@RunWith(JUnit4::class)
class CreditReportNetworkDataSourceTest : BaseNetworkDataSourceTest() {
    private lateinit var dataSource: CreditReportNetworkDataSource

    @Before
    fun setup() {
        dataSource = CreditReportNetworkDataSource(
            createApiService(ICreditReportApiService::class)
        )
    }

    @Test
    fun `should return a credit report given a valid 200 response`() {
        // given
        mockWebServer.enqueueResponse(fileName = "credit-report-200-valid", code = 200)
        // when
        val actual = dataSource.getCreditReport()
        // then
        assertEquals(tCreditReportDto, actual)
    }

    @Test
    fun `should return a null credit report given an invalid 200 response`() {
        // given
        mockWebServer.enqueueResponse(fileName = "credit-report-200-invalid", code = 200)
        // when
        val actual = dataSource.getCreditReport()
        // then
        assertNull(actual)
    }

    @Test(expected = ServerException::class)
    fun `should throw a server exception given a non 200 response`() {
        // given
        mockWebServer.enqueueResponse(fileName = "credit-report-500", code = 500)
        // when
        dataSource.getCreditReport()
    }
}