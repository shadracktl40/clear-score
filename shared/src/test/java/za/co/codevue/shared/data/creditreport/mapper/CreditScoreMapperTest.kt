package za.co.codevue.shared.data.creditreport.mapper

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.shared.data.tCreditReportDto
import za.co.codevue.shared.data.tCreditReportDtoWithNullValues

@RunWith(JUnit4::class)
class CreditScoreMapperTest {
    @Test
    fun `should map CreditReportDTO to CreditScore`() {
        // given
        val dto = tCreditReportDto
        // when
        val actual = CreditScoreMapper.toModel(dto)
        // then
        with(actual) {
            assertEquals(dto.changedScore, changedScore)
            assertEquals(dto.score, currentScore)
            assertEquals(dto.maxScoreValue, maxScore)
            assertEquals(dto.minScoreValue, minScore)
        }
    }

    @Test
    fun `should map CreditReportDTO to CreditScore with default values`() {
        // given
        val dto = tCreditReportDtoWithNullValues
        // when
        val actual = CreditScoreMapper.toModel(dto)
        // then
        with(actual) {
            assertEquals(0, changedScore)
            assertEquals(0, currentScore)
            assertEquals(0, maxScore)
            assertEquals(0, minScore)
        }
    }
}