package za.co.codevue.shared.data.creditreport.mapper

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.shared.data.tCreditReportDto
import za.co.codevue.shared.data.tCreditReportDtoWithNullValues
import za.co.codevue.shared.util.toSafeBigDecimal
import java.math.BigDecimal

@RunWith(JUnit4::class)
class CreditReportMapperTest {
    @Test
    fun `should map CreditReportDTO to CreditReport`() {
        // given
        val dto = tCreditReportDto
        // when
        val actual = CreditReportMapper.toModel(dto)
        // then
        with(actual) {
            // credit score
            with(creditScore) {
                assertEquals(dto.changedScore, changedScore)
                assertEquals(dto.score, currentScore)
                assertEquals(dto.maxScoreValue, maxScore)
                assertEquals(dto.minScoreValue, minScore)
            }
            assertEquals(dto.daysUntilNextReport, daysUntilNextReport)
            assertEquals(dto.hasEverDefaulted, hasEverDefaulted)
            // long term debt
            with(longTermDebt) {
                assertEquals(
                    dto.changeInLongTermDebt?.toSafeBigDecimal(),
                    changeInDebt
                )
                assertNull(creditLimit)
                assertNull(creditUtilization)
                assertEquals(
                    dto.currentLongTermDebt?.toSafeBigDecimal(),
                    totalDebt
                )
            }
            assertEquals(dto.numNegativeScoreFactors, negativeFactors)
            assertEquals(dto.numPositiveScoreFactors, positiveFactors)
            assertEquals(dto.percentageCreditUsed, percentageCreditUsed)
            assertEquals(dto.equifaxScoreBandDescription, scoreBandDescription)
            // short term debt
            with(shortTermDebt) {
                assertEquals(
                    dto.changeInShortTermDebt?.toSafeBigDecimal(),
                    changeInDebt
                )
                assertEquals(
                    dto.currentShortTermCreditLimit?.toSafeBigDecimal(),
                    creditLimit
                )
                assertEquals(
                    dto.currentShortTermCreditUtilisation,
                    creditUtilization
                )
                assertEquals(
                    dto.currentShortTermDebt?.toSafeBigDecimal(),
                    totalDebt
                )
            }
        }
    }

    @Test
    fun `should map CreditReportDTO to CreditReport with default values`() {
        // given
        val dto = tCreditReportDtoWithNullValues
        // when
        val actual = CreditReportMapper.toModel(dto)
        // then
        with(actual) {
            // credit score
            with(creditScore) {
                assertEquals(0, changedScore)
                assertEquals(0, currentScore)
                assertEquals(0, maxScore)
                assertEquals(0, minScore)
            }
            assertEquals(0, daysUntilNextReport)
            assertEquals(false, hasEverDefaulted)
            // long term debt
            with(longTermDebt) {
                assertEquals(BigDecimal.ZERO, changeInDebt)
                assertNull(creditLimit)
                assertNull(creditUtilization)
                assertEquals(BigDecimal.ZERO, changeInDebt)
            }
            assertEquals(0, negativeFactors)
            assertEquals(0, positiveFactors)
            assertEquals(0, percentageCreditUsed)
            assertEquals("", scoreBandDescription)
            // short term debt
            with(shortTermDebt) {
                assertEquals(BigDecimal.ZERO, changeInDebt)
                assertEquals(BigDecimal.ZERO, creditLimit)
                assertEquals(0, creditUtilization)
                assertEquals(BigDecimal.ZERO, changeInDebt)
            }
        }
    }
}