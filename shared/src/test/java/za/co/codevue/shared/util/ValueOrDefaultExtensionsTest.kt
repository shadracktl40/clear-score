package za.co.codevue.shared.util

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.math.BigDecimal


@RunWith(JUnit4::class)
class ValueOrDefaultExtensionsTest {

    @Test
    fun `should not return defaults when values are set`() {
        // arrange
        val nullableString = "test" as String?
        val nullableInt = 10 as Int?
        val nullableBoolean = true as Boolean?
        val nullableBigDecimal = BigDecimal(10) as BigDecimal?
        // assert
        assertEquals(nullableString, nullableString.valueOrDefault())
        assertEquals(nullableInt, nullableInt.valueOrDefault())
        assertEquals(nullableBoolean, nullableBoolean.valueOrDefault())
        assertEquals(nullableBigDecimal, nullableBigDecimal.valueOrDefault())
    }

    @Test
    fun `should return defaults when values are null`() {
        // arrange
        val nullableString: String? = null
        val nullableInt: Int? = null
        val nullableBoolean: Boolean? = null
        val nullableBigDecimal: BigDecimal? = null
        // assert
        assertTrue(
            "Default String value must be blank",
            nullableString.valueOrDefault().isBlank()
        )
        assertTrue(
            "Default Int value must be zero",
            nullableInt.valueOrDefault() == 0
        )
        assertFalse(
            "Default Boolean value must be false",
            nullableBoolean.valueOrDefault()
        )
        assertTrue(
            "Default BigDecimal value must be zero",
            nullableBigDecimal.valueOrDefault() == BigDecimal.ZERO
        )
    }
}