package za.co.codevue.shared.domain.creditreport

import androidx.lifecycle.MutableLiveData
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import za.co.codevue.androidtest.shared.util.getOrAwaitValue
import za.co.codevue.model.CreditReport
import za.co.codevue.shared.data.creditreport.repository.ICreditReportRepository
import za.co.codevue.shared.domain.BaseUseCaseTest
import za.co.codevue.shared.result.Result
import za.co.codevue.shared.result.failed
import za.co.codevue.shared.result.succeeded
import za.co.codevue.test.shared.data.tCreditReport
import java.lang.Exception

@RunWith(JUnit4::class)
class GetCreditReportUseCaseTest : BaseUseCaseTest() {
    private val repository: ICreditReportRepository = mock()

    @Test
    fun `should return a credit report`() {
        // given
        val resultMutableLiveData = MutableLiveData<Result<CreditReport>>()
        val useCase = createUseCase(repository)
        whenever(repository.getCreditReport()).thenReturn(tCreditReport)
        // when
        useCase.invoke(Unit, resultMutableLiveData)
        // then
        val actual = resultMutableLiveData.getOrAwaitValue()
        assertTrue(actual.succeeded)
        assertEquals(tCreditReport, (actual as Result.Success).data)
        verify(repository, times(numInvocations = 1)).getCreditReport()
    }

    @Test
    fun `should return an error when getCreditReport throws an exception`() {
        // given
        val resultMutableLiveData = MutableLiveData<Result<CreditReport>>()
        val errorMessage = "test error"
        val useCase = createUseCase(repository)
        whenever(repository.getCreditReport()).thenAnswer {
            throw Exception(errorMessage)
        }
        // when
        useCase.invoke(Unit, resultMutableLiveData)
        // then
        val actual = resultMutableLiveData.getOrAwaitValue()
        assertTrue(actual.failed)
        assertEquals(errorMessage, (actual as Result.Error).message)
        verify(repository, times(numInvocations = 1)).getCreditReport()
    }

    private fun createUseCase(
        repository: ICreditReportRepository
    ) = GetCreditReportUseCase(repository)
}