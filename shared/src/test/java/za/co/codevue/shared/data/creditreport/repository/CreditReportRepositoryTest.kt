package za.co.codevue.shared.data.creditreport.repository

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import za.co.codevue.shared.data.creditreport.datasource.ICreditReportDataSource
import za.co.codevue.shared.data.creditreport.mapper.CreditReportMapper
import za.co.codevue.shared.data.tCreditReportDto

@RunWith(JUnit4::class)
class CreditReportRepositoryTest {

    @Test
    fun `should return a credit report on getCreditReport`() {
        // given
        val mockDataSource: ICreditReportDataSource = mock {
            on { getCreditReport() } doReturn tCreditReportDto
        }
        val repository = CreditReportRepository(mockDataSource)
        // when
        val actual = repository.getCreditReport()
        val expected = CreditReportMapper.toModel(tCreditReportDto)
        // then
        assertEquals(expected, actual)
        verify(mockDataSource, times(numInvocations = 1)).getCreditReport()
    }
}