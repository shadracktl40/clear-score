package za.co.codevue.shared.data.creditreport.mapper

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import za.co.codevue.shared.data.tCreditReportDto
import za.co.codevue.shared.data.tCreditReportDtoWithNullValues
import za.co.codevue.shared.util.toSafeBigDecimal
import java.math.BigDecimal

@RunWith(JUnit4::class)
class LongTermCreditDebtMapperTest {
    @Test
    fun `should map CreditReportDTO to CreditDebt`() {
        // given
        val dto = tCreditReportDto
        // when
        val actual = LongTermCreditDebtMapper.toModel(dto)
        // then
        with(actual) {
            assertEquals(
                dto.changeInLongTermDebt?.toSafeBigDecimal(),
                changeInDebt
            )
            assertNull(creditLimit)
            assertNull(creditUtilization)
            assertEquals(
                dto.currentLongTermDebt?.toSafeBigDecimal(),
                totalDebt
            )
        }
    }

    @Test
    fun `should map CreditReportDTO to CreditDebt with default values`() {
        // given
        val dto = tCreditReportDtoWithNullValues
        // when
        val actual = LongTermCreditDebtMapper.toModel(dto)
        // then
        with(actual) {
            assertEquals(BigDecimal.ZERO, changeInDebt)
            assertNull(creditLimit)
            assertNull(creditUtilization)
            assertEquals(BigDecimal.ZERO, changeInDebt)
        }
    }
}