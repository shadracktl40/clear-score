package za.co.codevue.shared.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule
import za.co.codevue.shared.util.SyncTaskExecutorRule

abstract class BaseUseCaseTest {
    // Executes tasks in the Architecture Components in the same thread
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Executes tasks in a synchronous [TaskScheduler]
    @get:Rule
    val syncTaskExecutorRule = SyncTaskExecutorRule()
}