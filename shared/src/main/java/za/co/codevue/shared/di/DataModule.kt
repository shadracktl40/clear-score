package za.co.codevue.shared.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import za.co.codevue.shared.api.creditreport.ICreditReportApiService
import za.co.codevue.shared.data.creditreport.datasource.CreditReportNetworkDataSource
import za.co.codevue.shared.data.creditreport.datasource.ICreditReportDataSource
import za.co.codevue.shared.data.creditreport.repository.CreditReportRepository
import za.co.codevue.shared.data.creditreport.repository.ICreditReportRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {
    @Provides
    @Singleton
    fun provideCreditReportNetworkDataSource(
        apiService: ICreditReportApiService
    ): ICreditReportDataSource {
        return CreditReportNetworkDataSource(apiService)
    }

    @Provides
    @Singleton
    fun provideCreditReportRepository(
        dataSource: ICreditReportDataSource
    ): ICreditReportRepository {
        return CreditReportRepository(dataSource)
    }
}