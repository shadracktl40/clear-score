package za.co.codevue.shared.parcelize

import android.os.Parcel
import kotlinx.parcelize.Parceler
import za.co.codevue.model.CreditDebt
import za.co.codevue.shared.util.readBigDecimal
import za.co.codevue.shared.util.valueOrDefault
import za.co.codevue.shared.util.writeBigDecimal

/**
 * A [Parceler] mapping object for [CreditDebt]
 */
object CreditDebtParceler : Parceler<CreditDebt> {
    override fun create(parcel: Parcel): CreditDebt {
        return CreditDebt(
            parcel.readBigDecimal(),
            parcel.readBigDecimal(),
            parcel.readInt(),
            parcel.readBigDecimal()
        )
    }

    override fun CreditDebt.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeBigDecimal(changeInDebt)
            writeBigDecimal(creditLimit)
            writeInt(creditUtilization.valueOrDefault())
            writeBigDecimal(totalDebt)
        }
    }
}