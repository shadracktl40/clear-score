package za.co.codevue.shared.data.creditreport.datasource

import za.co.codevue.shared.api.creditreport.ICreditReportApiService
import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO
import za.co.codevue.shared.util.fetch

internal class CreditReportNetworkDataSource(
    private val apiService: ICreditReportApiService
) : ICreditReportDataSource {
    override fun getCreditReport(): CreditReportDTO {
        return apiService.getCreditReport().fetch().data
    }
}