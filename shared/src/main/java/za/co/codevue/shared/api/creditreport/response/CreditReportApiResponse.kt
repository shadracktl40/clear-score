package za.co.codevue.shared.api.creditreport.response

import com.google.gson.annotations.SerializedName
import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO

data class CreditReportApiResponse(
    @SerializedName(value = "creditReportInfo")
    val data: CreditReportDTO
)
