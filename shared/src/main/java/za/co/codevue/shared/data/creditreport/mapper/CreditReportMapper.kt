package za.co.codevue.shared.data.creditreport.mapper

import za.co.codevue.model.CreditReport
import za.co.codevue.shared.data.BaseMapper
import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO
import za.co.codevue.shared.util.valueOrDefault

object CreditReportMapper : BaseMapper<CreditReport, CreditReportDTO> {
    override fun toModel(dto: CreditReportDTO): CreditReport {
        return CreditReport(
            CreditScoreMapper.toModel(dto),
            dto.daysUntilNextReport.valueOrDefault(),
            dto.hasEverDefaulted.valueOrDefault(),
            LongTermCreditDebtMapper.toModel(dto),
            dto.numNegativeScoreFactors.valueOrDefault(),
            dto.numPositiveScoreFactors.valueOrDefault(),
            dto.percentageCreditUsed.valueOrDefault(),
            dto.equifaxScoreBandDescription.valueOrDefault(),
            ShortTermCreditDebtMapper.toModel(dto)
        )
    }
}