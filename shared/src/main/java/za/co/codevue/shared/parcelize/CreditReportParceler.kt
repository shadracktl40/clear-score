package za.co.codevue.shared.parcelize

import android.os.Parcel
import kotlinx.parcelize.Parceler
import za.co.codevue.model.CreditReport
import za.co.codevue.shared.parcelize.CreditDebtParceler.write
import za.co.codevue.shared.parcelize.CreditScoreParceler.write
import za.co.codevue.shared.util.readBool
import za.co.codevue.shared.util.valueOrDefault
import za.co.codevue.shared.util.writeBool

/**
 * A [Parceler] mapping object for [CreditReport]
 */
object CreditReportParceler : Parceler<CreditReport> {
    override fun create(parcel: Parcel): CreditReport {
        return CreditReport(
            CreditScoreParceler.create(parcel),
            parcel.readInt(),
            parcel.readBool(),
            CreditDebtParceler.create(parcel),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString().valueOrDefault(),
            CreditDebtParceler.create(parcel),
        )
    }

    override fun CreditReport.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            creditScore.write(parcel, flags)
            writeInt(daysUntilNextReport)
            writeBool(hasEverDefaulted)
            longTermDebt.write(parcel, flags)
            writeInt(negativeFactors)
            writeInt(positiveFactors)
            writeInt(percentageCreditUsed)
            writeString(scoreBandDescription)
            shortTermDebt.write(parcel, flags)
        }
    }
}