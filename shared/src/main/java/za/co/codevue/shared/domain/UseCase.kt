package za.co.codevue.shared.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import za.co.codevue.shared.result.Result
import za.co.codevue.shared.util.DefaultScheduler
import za.co.codevue.shared.util.logException

/**
 * Executes business logic synchronously or asynchronously using a [Scheduler].
 */
abstract class UseCase<in P, R> {

    private val taskScheduler = DefaultScheduler

    /** Executes the use case asynchronously and places the [Result] in a MutableLiveData
     *
     * @param parameters the input parameters to run the use case with
     * @param result the MutableLiveData where the result is posted to
     *
     */
    operator fun invoke(parameters: P, result: MutableLiveData<Result<R>>) {
        result.value = Result.Loading()
        try {
            taskScheduler.execute {
                try {
                    execute(parameters).let { useCaseResult ->
                        result.postValue(Result.Success(useCaseResult))
                    }
                } catch (exception: Exception) {
                    result.postValue(getError(exception.logException()))
                }
            }
        } catch (exception: Exception) {
            result.postValue(getError(exception.logException()))
        }
    }

    /** Executes the use case asynchronously and returns a [Result] in a new LiveData object.
     *
     * @return an observable [LiveData] with a [Result].
     *
     * @param parameters the input parameters to run the use case with
     */
    operator fun invoke(parameters: P): LiveData<Result<R>> {
        val liveCallback: MutableLiveData<Result<R>> = MutableLiveData()
        this(parameters, liveCallback)
        return liveCallback
    }

    /** Executes the use case synchronously  */
    fun executeNow(parameters: P): Result<R> {
        return try {
            Result.Success(execute(parameters))
        } catch (exception: Exception) {
            getError(exception.logException())
        }
    }

    /**
     * Override this to set the code to be executed.
     */
    @Throws(RuntimeException::class)
    protected abstract fun execute(parameters: P): R

    /**
     * Get Result error based on exception type
     */
    private fun getError(exception: Exception): Result<R> {
        with(exception) {
            return when (this) {
                is Result.ServerException -> Result.Server(statusCode, errorMessage, errorCode)
                is Result.NetworkException -> Result.Network(error)
                else -> Result.Unknown(this.message ?: "An error occurred, please try again.")
            }
        }
    }
}

operator fun <R> UseCase<Unit, R>.invoke(): LiveData<Result<R>> = this(Unit)
operator fun <R> UseCase<Unit, R>.invoke(result: MutableLiveData<Result<R>>) = this(Unit, result)
