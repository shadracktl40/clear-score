package za.co.codevue.shared.api

data class ErrorApiResponse(
    val code: Int?,
    val message: String?
)
