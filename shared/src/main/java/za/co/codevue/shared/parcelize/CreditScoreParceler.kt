package za.co.codevue.shared.parcelize

import android.os.Parcel
import kotlinx.parcelize.Parceler
import za.co.codevue.model.CreditScore

/**
 * A [Parceler] mapping object for [CreditScore]
 */
object CreditScoreParceler : Parceler<CreditScore> {
    override fun create(parcel: Parcel): CreditScore {
        return CreditScore(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()
        )
    }

    override fun CreditScore.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeInt(changedScore)
            writeInt(currentScore)
            writeInt(maxScore)
            writeInt(minScore)
        }
    }
}