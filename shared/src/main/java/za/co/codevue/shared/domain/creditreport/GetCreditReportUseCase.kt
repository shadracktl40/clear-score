package za.co.codevue.shared.domain.creditreport

import za.co.codevue.model.CreditReport
import za.co.codevue.shared.data.creditreport.repository.ICreditReportRepository
import za.co.codevue.shared.domain.UseCase
import javax.inject.Inject

open class GetCreditReportUseCase @Inject constructor(
    private val repository: ICreditReportRepository
): UseCase<Unit, CreditReport>() {
    override fun execute(parameters: Unit): CreditReport {
        return repository.getCreditReport()
    }
}