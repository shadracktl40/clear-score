package za.co.codevue.shared.api

object ApiConstants {
    const val CONNECTION_READ_TIMEOUT = 60L
    const val BASE_API_URL = "https://android-interview.s3.eu-west-2.amazonaws.com/"
    const val CREDIT_REPORT_API_ENDPOINT = "endpoint.json"
}