package za.co.codevue.shared.data.creditreport.mapper

import za.co.codevue.model.CreditDebt
import za.co.codevue.shared.data.BaseMapper
import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO
import za.co.codevue.shared.util.toSafeBigDecimal
import za.co.codevue.shared.util.valueOrDefault

object ShortTermCreditDebtMapper : BaseMapper<CreditDebt, CreditReportDTO> {
    override fun toModel(dto: CreditReportDTO): CreditDebt {
        return CreditDebt(
            dto.changeInShortTermDebt.valueOrDefault().toSafeBigDecimal(),
            dto.currentShortTermCreditLimit.valueOrDefault().toSafeBigDecimal(),
            dto.currentShortTermCreditUtilisation.valueOrDefault(),
            dto.currentShortTermDebt.valueOrDefault().toSafeBigDecimal()
        )
    }
}