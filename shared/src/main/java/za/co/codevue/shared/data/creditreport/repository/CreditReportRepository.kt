package za.co.codevue.shared.data.creditreport.repository

import za.co.codevue.model.CreditReport
import za.co.codevue.shared.data.creditreport.datasource.ICreditReportDataSource
import za.co.codevue.shared.data.creditreport.mapper.CreditReportMapper

internal class CreditReportRepository(
    private val dataSource: ICreditReportDataSource
) : ICreditReportRepository {
    override fun getCreditReport(): CreditReport {
        return dataSource.getCreditReport().run {
            CreditReportMapper.toModel(this)
        }
    }
}