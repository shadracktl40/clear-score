package za.co.codevue.shared.data

internal interface BaseMapper<Model, DTO> {
    fun toModel(dto: DTO) : Model
}