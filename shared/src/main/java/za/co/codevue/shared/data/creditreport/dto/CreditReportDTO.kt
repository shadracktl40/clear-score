package za.co.codevue.shared.data.creditreport.dto

data class CreditReportDTO(
    val changeInLongTermDebt: String?,
    val changeInShortTermDebt: String?,
    val changedScore: Int?,
    val currentLongTermCreditLimit: String?,
    val currentLongTermCreditUtilisation: String?,
    val currentLongTermDebt: String?,
    val currentShortTermCreditLimit: String?,
    val currentShortTermCreditUtilisation: Int?,
    val currentShortTermDebt: String?,
    val daysUntilNextReport: Int?,
    val equifaxScoreBandDescription: String?,
    val hasEverDefaulted: Boolean?,
    val maxScoreValue: Int?,
    val minScoreValue: Int?,
    val monthsSinceLastDefaulted: Int?,
    val numNegativeScoreFactors: Int?,
    val numPositiveScoreFactors: Int?,
    val percentageCreditUsed: Int?,
    val score: Int?
)