package za.co.codevue.shared.data.creditreport.repository

import za.co.codevue.model.CreditReport

interface ICreditReportRepository {
    fun getCreditReport(): CreditReport
}