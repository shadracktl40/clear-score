package za.co.codevue.shared.api.creditreport

import retrofit2.Call
import retrofit2.http.GET
import za.co.codevue.shared.api.ApiConstants
import za.co.codevue.shared.api.creditreport.response.CreditReportApiResponse

interface ICreditReportApiService {
    @GET(value = ApiConstants.CREDIT_REPORT_API_ENDPOINT)
    fun getCreditReport(): Call<CreditReportApiResponse>
}