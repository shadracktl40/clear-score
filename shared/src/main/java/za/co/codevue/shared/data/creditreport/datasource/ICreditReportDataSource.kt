package za.co.codevue.shared.data.creditreport.datasource

import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO

interface ICreditReportDataSource {
    fun getCreditReport(): CreditReportDTO
}