package za.co.codevue.shared.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import za.co.codevue.shared.BuildConfig
import za.co.codevue.shared.api.ApiConstants
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    private const val OK_HTTP_TAG = "OkHttp"

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            // Set timeouts
            connectTimeout(ApiConstants.CONNECTION_READ_TIMEOUT, TimeUnit.SECONDS)
            readTimeout(ApiConstants.CONNECTION_READ_TIMEOUT, TimeUnit.SECONDS)

            // Add request and response logging
            if (BuildConfig.DEBUG) {
                val logging = HttpLoggingInterceptor { message ->
                    Timber.tag(OK_HTTP_TAG).d(message)
                }
                logging.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(logging)
            }
        }.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().baseUrl(ApiConstants.BASE_API_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}