package za.co.codevue.shared.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import za.co.codevue.shared.api.creditreport.ICreditReportApiService
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    @Provides
    @Singleton
    fun provideCreditReportApiService(retrofit: Retrofit): ICreditReportApiService {
        return retrofit.create(ICreditReportApiService::class.java)
    }
}