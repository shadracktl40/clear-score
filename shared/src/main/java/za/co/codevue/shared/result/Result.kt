package za.co.codevue.shared.result

import androidx.lifecycle.MutableLiveData

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Result<out R> {
    // Exception types
    class ServerException(
        val statusCode: Int,
        val errorMessage: String,
        val errorCode: String? = null
    ) : Exception()

    class NetworkException(val error: String) : Exception()

    // Base Error
    abstract class Error(val message: String) : Result<Nothing>()

    // Result types
    class Loading(val isLoading: Boolean = true) : Result<Nothing>()

    class Loaded(val isLoaded: Boolean = true) : Result<Nothing>()

    data class Success<out T>(val data: T) : Result<T>()
    class Server(val statusCode: Int, message: String, val errorCode: String?) : Error(message)
    class Network(message: String) : Error(message)
    class Unknown(message: String) : Error(message)

    override fun toString(): String {
        return when (this) {
            is Loading -> "Loading[data=$isLoading]"
            is Loaded -> "Loaded[data=$isLoaded]"
            is Success<*> -> "Success[data=$data]"
            is Server -> "Server[statusCode=$statusCode, errorCode=$errorCode message=$message]"
            is Network -> "Network[message=$message]"
            is Unknown -> "Unknown[message=$message]"
            is Error -> "Error[message=$message]"
        }
    }
}

/**
 * `true` if [Result] is of type [Result.Loading] & [Result.Loading.isLoading] is `true`.
 */
val Result<*>.isLoading
    get() = this is Result.Loading && isLoading

/**
 * `true` if [Result] is of type [Result.Success] & holds non-null [Result.Success.data].
 */
val Result<*>.succeeded
    get() = this is Result.Success && data != null

/**
 * `true` if [Result] is of type [Result.Error].
 */
val Result<*>.failed
    get() = this is Result.Error

/**
 * Updates value of [liveData] if [Result] is of type [Success]
 */
inline fun <reified T> Result<T>.updateOnSuccess(liveData: MutableLiveData<T>) {
    if (this is Result.Success) {
        liveData.value = data
    }
}

/**
 * Updates value of [liveData] if [Result] is of type [Result.Loading]
 */
fun <T> Result<T>.updateOnLoading(liveData: MutableLiveData<Boolean>) {
    liveData.value = isLoading
}

/**
 * Updates value of [liveData] if [Result] is of type [Result.Error]
 */
fun <T> Result<T>.updateOnFailure(liveData: MutableLiveData<Result.Error?>) {
    liveData.value = if (failed) this as Result.Error else null
}