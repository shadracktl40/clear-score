package za.co.codevue.shared.data.creditreport.mapper

import za.co.codevue.model.CreditDebt
import za.co.codevue.shared.data.BaseMapper
import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO
import za.co.codevue.shared.util.toSafeBigDecimal
import za.co.codevue.shared.util.valueOrDefault

object LongTermCreditDebtMapper : BaseMapper<CreditDebt, CreditReportDTO> {
    override fun toModel(dto: CreditReportDTO): CreditDebt {
        return CreditDebt(
            changeInDebt = dto.changeInLongTermDebt.valueOrDefault().toSafeBigDecimal(),
            totalDebt = dto.currentLongTermDebt.valueOrDefault().toSafeBigDecimal()
        )
    }
}