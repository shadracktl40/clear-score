package za.co.codevue.shared.data.creditreport.mapper

import za.co.codevue.model.CreditScore
import za.co.codevue.shared.data.BaseMapper
import za.co.codevue.shared.data.creditreport.dto.CreditReportDTO
import za.co.codevue.shared.util.valueOrDefault

object CreditScoreMapper : BaseMapper<CreditScore, CreditReportDTO> {
    override fun toModel(dto: CreditReportDTO): CreditScore {
        return CreditScore(
            dto.changedScore.valueOrDefault(),
            dto.score.valueOrDefault(),
            dto.maxScoreValue.valueOrDefault(),
            dto.minScoreValue.valueOrDefault()
        )
    }
}