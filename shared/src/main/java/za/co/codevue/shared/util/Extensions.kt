package za.co.codevue.shared.util

import android.os.Parcel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.google.gson.Gson
import com.google.gson.JsonParseException
import retrofit2.Call
import timber.log.Timber
import za.co.codevue.shared.api.ErrorApiResponse
import za.co.codevue.shared.result.Result
import java.math.BigDecimal
import java.math.BigInteger
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

/** Uses `Transformations.map` on a LiveData */
fun <X, Y> LiveData<X>.map(body: (X) -> Y): LiveData<Y> {
    return Transformations.map(this, body)
}

/** Makes a synchronous Retrofit call without callbacks */
fun <ResponseType : Any> Call<ResponseType>.fetch(): ResponseType {
    try {
        val response = this.execute()
        if (response.isSuccessful) {
            return response.body()!!
        } else {
            /** Try parsing error body to [ErrorApiResponse] */
            try {
                val errorResponse: ErrorApiResponse? = Gson().fromJson(
                    response.errorBody()?.string(),
                    ErrorApiResponse::class.java
                )

                if (errorResponse != null) {
                    throw Result.ServerException(
                        response.code(),
                        errorResponse.message.valueOrDefault()
                    )
                } else {
                    throw Result.ServerException(response.code(), response.message())
                }
            } catch (exception: Exception) {
                if (exception is Result.ServerException) {
                    throw exception
                } else {
                    throw Result.ServerException(response.code(), exception.toString())
                }
            }
        }
    } catch (exception: Exception) {
        throw exception
    }
}

/** Exception helper for Crashlytics */
fun Exception.logException(): Exception {
    with(this) {
        when (this) {
            is Result.ServerException -> {
                if (statusCode !in 200..300) {
                    logException("HTTP", "Status Code: $statusCode", Exception(message))
                }
            }
            is NullPointerException -> {
                logException("NullPointerException", message, this)
            }
            is JsonParseException -> {
                logException("JsonParseException", message, this)
            }
            is ConnectException,
            is UnknownHostException -> {
                logException("ConnectException", message, this)
                return Result.NetworkException("Please check your internet settings and try again.")
            }
            is SocketTimeoutException -> {
                logException("SocketTimeoutException", message, this)
                return Result.NetworkException("Request timed out, please try again.")
            }
            else -> {
                logException("UnknownException", message, this)
            }
        }
        return this
    }
}

/** Logs an exception with Timber */
private fun logException(tag: String, message: String?, throwable: Throwable?) {
    Timber.tag(tag).e(throwable, message)
}

/**
 * Returns the value of [String] if not null or a blank [String]
 */
fun String?.valueOrDefault() = this ?: ""

/**
 * Returns the value of [Int] if not null or zero
 */
fun Int?.valueOrDefault() = this ?: 0

/**
 * Returns the value of [Boolean] if not null or false
 */
fun Boolean?.valueOrDefault() = this ?: false

/**
 * Returns the value of [BigDecimal] if not null or a [BigDecimal] with a value of zero
 */
fun BigDecimal?.valueOrDefault() = this ?: BigDecimal(0)

/** Converts a [String] to [BigDecimal] safely */
fun String.toSafeBigDecimal(): BigDecimal {
    return try {
        BigDecimal(this.replace(",", "").trim())
    } catch (e: Exception) {
        Timber.e(e)
        BigDecimal(0)
    }
}

/** Read a nullable value from [Parcel] */
inline fun <T> Parcel.readNullable(reader: () -> T) = if (readInt() != 0) reader() else null

/** Write a nullable value to [Parcel] */
inline fun <T> Parcel.writeNullable(value: T?, writer: (T) -> Unit) {
    if (value != null) {
        writeInt(1)
        writer(value)
    } else {
        writeInt(0)
    }
}

/** Write a nullable [BigDecimal] to [Parcel] */
fun Parcel.writeBigDecimal(value: BigDecimal?) = writeNullable(value) {
    writeByteArray(it.unscaledValue().toByteArray())
    writeInt(it.scale())
}

/** Read a nullable [BigDecimal] from [Parcel] */
fun Parcel.readBigDecimal() = readNullable {
    BigDecimal(BigInteger(createByteArray()), readInt())
} ?: BigDecimal(0)

/** Read a [Boolean] from [Parcel] */
fun Parcel.writeBool(value: Boolean) {
    writeInt(if (value) 1 else 0)
}

/** Write a [Boolean] to [Parcel] */
fun Parcel.readBool(): Boolean {
    return readInt() == 1
}

/**
 * Formats a [BigDecimal] to a currency [String] i.e R 1 000.00
 */
fun BigDecimal?.format(): String {
    val symbols = DecimalFormatSymbols().apply {
        groupingSeparator = ' '
        decimalSeparator = '.'
    }
    val decimalFormatPattern = when {
        this.valueOrDefault() < BigDecimal(1) -> "0.00"
        else -> "#,###.00"
    }
    return DecimalFormat(
        "R $decimalFormatPattern",
        symbols
    ).format(this.valueOrDefault())
}