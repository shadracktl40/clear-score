# Clear Score Assessment

App that displays the credit score information of a user.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="/assets/screen_overview.png" width="40%" height="40%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="/assets/screen_details.png" width="40%" height="40%">

## Tech stack
- Kotlin
- Android Architecture Components
- Dagger Hilt
- Retrofit
- Timber

## Architecture

### Multi modular project structure
![Architecture](/assets/clean_architecture.jpg "Clean Architecture")

This project aims for showcasing a clean architecture based on 3 concepts: App, Model and Shared modules

- **App** contains all user facing functionality (e.g. Credit Report Overview(donut), Credit Report Details)
- **Model** contains all the models  
- **Shared** contains all the logic for features (e.g. use cases, network)

## Testing strategy

#### *Api Layer*
The api layer is tested through a combination of unit tests + MockWebServer in order to fully test Retrofit network integrations. By following  this approach we are covering:
- Retrofit instance
- Json converters, in this case Kotlin Serialization converters
- Network data source

#### *Data Layer*
- Unit tests for the Repositories and Mappers

#### *Domain Layer*
- Unit tests for the Use Cases

#### *Presentation Layer*
- Unit tests for View Models
